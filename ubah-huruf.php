<?php
function ubah_huruf($string){
    //kode di sini
    $letter =  ['t', 'w', 'e', 'd', 'v', 'p', 'o', 'a', 'n', 's', 'm', 'l', 'g', 'r', 'k'];
    $replace = ['u', 'x', 'f', 'e', 'w', 'q', 'p', 'b', 'o', 't', 'n', 'm', 'h', 's', 'l'];

    return str_replace($letter, $replace, $string);
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo '<br>';
echo ubah_huruf('developer'); // efwfmpqfs
echo '<br>';
echo ubah_huruf('laravel'); // mbsbwfm
echo '<br>';
echo ubah_huruf('keren'); // lfsfo
echo '<br>';
echo ubah_huruf('semangat'); // tfnbohbu

?>